<?php

/* 
 * @file
 * The class file of whistlechat public open platform.
 * 微哨开放平台类文件
 */
error_reporting(E_ALL & ~(E_STRICT | E_NOTICE));
require('include/Whistlechat.php');

class Whistle extends Whistlechat {
  /**
   * 微哨应用服务器地址（域名） 
   * @var string
   */
  protected $appserver;
  
  /**
   * 返回微哨应用服务器地址（域名）
   * @return string
   */
  public function getAppserver() {
    return $this->appserver;
  }

  /**
   * 构造函数，添加微哨服务器地域（域名）
   * @param string $appid
   * @param string $appsecret
   * @param string $appserver
   * @param string $apptoken
   */
  public function __construct($appid, $appsecret, $appserver, $apptoken=NULL) {
    $this->appserver = $appserver;
    parent::__construct($appid, $appsecret, $apptoken);
  }

  /**
   * 调用钩子函数，返回第一个匹配模块的响应报文
   * 
   * @param Request $request 请求报文
   * @param string $messageType 收到消息类型
   * @return Response 返回第一个匹配模块钩子函数返回的响应报文
   */
  private function getResponse($request, $messageType) {
      $retArr = array();
      foreach (module_implements('revc_' . $messageType) as $module) {
        $function = $module . '_revc_' . $messageType;
        $retArr[] = $function($this, $request);
      }
      return $retArr[0];
  }
  
  /**
   * 调用钩子，用以实现父类Hello请求报文处理
   */
  protected function onRecvHelloMessage(HelloRequest $request) {
      return $this->getResponse($request, 'hello');
  }
  
  /**
   * 调用钩子，用以实现父类文本请求报文处理
   */
  protected function onRecvTextMessage(TextRequest $request) {
    return $this->getResponse($request, 'text');
  }
  
  /**
   * 调用钩子，用以实现父类图片请求报文处理
   */
  protected function onRecvImageMessage($request) {
    return $this->getResponse($request, 'image');
  }
  
  /**
   * 调用钩子，用以实现父类事件请求报文处理
   */
  protected function onRecvEventMessage($request) {
    return $this->getResponse($request, 'event');
  }
  
  /**
   * 调用钩子，用以实现父类位置请求报文处理
   */
  protected function onRecvLocationMessage($request) {
    return $this->getResponse($request, 'location');
  }
  
  /**
   * 调用钩子，用以实现父类未知请求报文处理
   */
  protected function onRecvUnknowMessage($request) {
    return $this->getResponse($request, 'unknow');
  }
}
