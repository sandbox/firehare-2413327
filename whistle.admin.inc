<?php

/**
 * @file
 * Forms for Whistle admin screens
 * 微哨管理页表单
 */

/**
 * Define the configure form.
 * 定义配置表单
 */
function whistle_admin_settings_form($form, $form_state) {
    $form['config'] = array(
      '#type' => 'fieldset',
      '#title' => t('Whistle Settings'),
	  '#collapsible' => TRUE,
	  '#collapsed' => FALSE,
    );
    // 设置轻应用通讯凭证
    $form['config']['whistle_config_apptoken']=array(
      '#type' => 'textfield',
      '#title' => t('Token'),
      '#description'=> t('The token set in whistle(whistle) public open platform.'),
      '#default_value' => variable_get('whistle_config_apptoken', ''),
      '#required' => TRUE,
    );
    // 设置轻应用ID
    $form['config']['whistle_config_appid']=array(
        '#type' => 'textfield',
        '#title' => t('AppID'),
        '#description'=> t('The appid given by whistle(whistle) public open platform.'),
        '#default_value' => variable_get('whistle_config_appid', ''),
        '#required' => FALSE,
    );
    //设置轻应用密钥
    $form['config']['whistle_config_appsecret']=array(
        '#type' => 'textfield',
        '#title' => t('AppSecret'),
        '#description'=> t('The app secret given by whistle public open platform.'),
        '#default_value' => variable_get('whistle_config_appsecret', ''),
        '#required' => FALSE,
    );
        // 设置微哨应用服务器
    $form['config']['whistle_config_appserver']=array(
        '#type' => 'textfield',
        '#title' => t('Ip Address'),
        '#description'=> t('The ip address or domain name of whistle application server. eg: X.X.X.X or www.example.com'),
        '#default_value' => variable_get('whistle_config_appserver', ''),
        '#required' => FALSE,
    );
    $form['#submit'][] = 'whistle_admin_settings_submit';
    /*
    $form['config']['submit_config'] = array(
      '#type' => 'submit',
      '#value' => t('save'),
      '#submit' => array('whistle_config_submit'),
    );
     */
    return system_settings_form($form);
} 

/**
 * Form submission handler for whistle_setting_form
 * 处理表单 whistle_settings_form 提交
 */
function whistle_admin_settings_submit($form, &$form_state){
  // 删除无关值
  form_state_values_clean($form_state);
  
  variable_set('whistle_config_apptoken', $form_state['values']['whistle_config_apptoken']);
  variable_set('whistle_config_appid', $form_state['values']['whistle_config_appid']);
  variable_set('whistle_config_appsecret', $form_state['values']['whistle_config_appsecret']);
  variable_set('whistle_config_appserver', $form_state['values']['whistle_config_appserver']);
}
