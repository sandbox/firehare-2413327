<?php

/**
 * @file
 * Documents API functions for whistle module.
 */

/**
 * Processing Hello request of the lightapp
 * 处理轻应用的 Hello 请求报文
 * 
 * 本函数示范在用户启动轻应用后，轻应用将自动显示用户信息
 * 
 * @param Whistle $whistleObj 表示 Whistle 对象
 * @param Request $request 表示 Hello 请求报文
 * @return Response 返回响应报文
 */
function hook_revc_hello(Whistle $whistleObj, Request $request) {
    $helloResponse = new TextResponse($request);
    $fromStudentNumber = $request->getFromStudentNumber();
    $url = 'https://'.$whistleObj->getAppserver().'/api/user/get_info';
    $userInfo = $whistleObj->getUserInfo($url, $fromStudentNumber);
    $content = "";
    foreach ($userInfo['data'] as $userInfo) {
      $content .= $userInfo . "<br>";
    }
    // $content = '尊敬的:' . $userInfo['data']['name'] . ',您好!这是Hello报文,以下是您的基本信息:' . '姓名：' . $userInfo['data']['name'] . ',性别:' . $userInfo['data']['sex'] . ',组织架构：' . $userInfo['data']['organization'] . ',输入1返回图文消息, 输入2返回文本信息';
    $helloResponse->setContent($content);
    return $helloResponse;
}

/**
 * Processing text request of the lightapp
 * 处理轻应用的文本请求报文
 * 
 * 本函数示范接受到用户回复文本后的不同处理，当用户回复1时，显示图文混排信息，回复2、3则显示文本提示消息
 * 
 * @param Whistle $whistleObj 表示 Whistle 对象
 * @param Request $request 表示文本请求报文
 * @return Response 返回响应报文
 */
function hook_revc_text(Whistle $whistleObj, Request $request) {
    $response = NULL;
    $content = $request->getContent();

    switch ($content) {
        case '1':
            $articles = new ArticleResponse($request);
            $arr = array();
            $arr[] = new Article("这是图文消息", "不要点我", "http://www.weishao.com.cn/upload/image/1385434253.png", "");
            $arr[] = new Article("第一点", "点我跳到微哨", "http://www.weishao.com.cn/upload/image/1385434408.png", "http://www.weishao.com.cn");
            $arr[] = new Article("第二点", "点我跳到百度", "http://www.weishao.com.cn/upload/image/1385693782.png", "http://www.baidu.com");
            $articles->setArticles($arr);
            $response = $articles;
            break;
        case '2':
            $text = new TextResponse($request);
            $textContent = "您好,按键3的功能就留给您实现吧!";
            $text->setContent($textContent);
            $response = $text;
            break;
        default;
            $help = new TextResponse($request);
            $helpContent = "您好,这是文本消息, 输入1返回图文消息, 输入2返回文本信息, 输入其他内容, 显示本消息!";
            $help->setContent($helpContent);
            $response = $help;
            break;
    }
    return $response;
}

/**
 * Processing image request of the lightapp
 * 处理轻应用的图像报文
 * 
 * 本函数示范用户发送图片后，显示一条提示文本信息。
 * 
 * @param Whistle $whistleObj 表示 Whistle 对象
 * @param Request $request 表示图像请求报文
 * @return Response 返回响应报文
 */
function hook_revc_image(Whistle $whistleObj, Request $request) {
    $textResponse = new TextResponse($request);
    $content = '这是图片消息';
    $textResponse->setContent($content);
    return $textResponse;
}

/**
 * Processing event request of the lightapp
 * 处理轻应用的事件报文
 * 
 * 本函数示范用户发送事件报文后，显示一条提示文本信息。
 *  
 * @param Whistle $whistleObj 表示 Whistle 对象
 * @param Request $request 表示事件请求报文
 * @return Response 返回响应报文
 */
function hook_revc_event(Whistle $whistleObj, Request $request){
        $textResponse = new TextResponse($request);
        $content = '这是事件消息';
        $textResponse->setContent($content);
        return $textResponse;
}

/**
 * Processing location request of the lightapp
 * 处理轻应用的位置报文
 * 
 * 本函数示范用户发送位置报文后，显示一条提示文本信息。
 *  
 * @param Whistle $whistleObj 表示 Whistle 对象
 * @param Request $request 表示位置请求报文
 * @return Response 返回响应报文
 */
function hook_revc_location(Whistle $whistleObj, Request $request){
        $textResponse = new TextResponse($request);
        $content = '这是位置消息';
        $textResponse->setContent($content);
        return $textResponse;
}

/**
 * Processing unknown request of the lightapp
 * 处理轻应用的未知报文
 * 
 * 本函数示范用户发送未知报文后，显示一条提示文本信息。
 *  
 * @param Whistle $whistleObj 表示 Whistle 对象
 * @param Request $request 表示未知请求报文
 * @return Response 返回响应报文
 */
function hook_revc_unknown(Whistle $whistleObj, Request $request){
        $textResponse = new TextResponse($request);
        $content = '这是未知消息';
        $textResponse->setContent($content);
        return $textResponse;
}
